const imgpath = './public/landing/assets/image/people/'

const people = [
    // {
    //     img: './public/landing/assets/image/people/people1.png',
    //     name: 'MURIO',
    //     position: 'Director, F4Elements',
    // },
    {
        img: './public/landing/assets/image/people/people2.png',
        name: 'AGIEL',
        position: 'Director, F4Elements',
    },
    {
        img: './public/landing/assets/image/people/people3.png',
        name: 'HENRY',
        position: 'Director, F4Elements',
    },
    {
        img: './public/landing/assets/image/people/people4.png',
        name: 'DHONO',
        position: 'Director, F4Elements',
    },
    {
        img: './public/landing/assets/image/people/people5.png',
        name: 'VANYA',
        position: 'Strategic Planner Head, F4Elements',
    },
    {
        img: './public/landing/assets/image/people/people6.png',
        name: 'ABDAH',
        position: 'Design Head, F4Elements',
    },
    {
        img: './public/landing/assets/image/people/people7.png',
        name: 'TUTIEK',
        position: 'Account Head, Interface',
    },
    {
        img: './public/landing/assets/image/people/people8.png',
        name: 'SHELLY',
        position: 'Account Head, Interface',
    },
    {
        img: './public/landing/assets/image/people/people9.png',
        name: 'INONG',
        position: 'Consumer Experience Head, Interface',
    },
    {
        img: './public/landing/assets/image/people/people10.png',
        name: 'BABA',
        position: 'Consumer Experience Head, Interface',
    },
    {
        img: './public/landing/assets/image/people/people11.png',
        name: 'YENDA',
        position: 'Head of Digital, Interface',
    },
    {
        img: './public/landing/assets/image/people/people12.png',
        name: 'MARTIN',
        position: 'Operation Head, Studio Terogong',
    },
    {
        img: './public/landing/assets/image/people/soraya.png',
        name: 'SORAYA',
        position: 'General Manager, Sunrise',
    },
    {
        img: './public/landing/assets/image/people/people14.png',
        name: 'ADIT',
        position: 'Operation Head, Sunrise',
    },
    {
        img: './public/landing/assets/image/people/people15.png',
        name: 'FARRAH',
        position: 'HR Business Partner, F4Elements',
    },
    {
        img: './public/landing/assets/image/people/people16.png',
        name: 'IDRIS',
        position: 'Production Head, Giza',
    },
    {
        img: './public/landing/assets/image/people/people17.png',
        name: 'LUSI',
        position: 'Sales & Operation Lead, Printworks',
    },
    {
        img: './public/landing/assets/image/people/people18.png',
        name: 'ITA',
        position: 'Sales Lead, Elements Branding',
    },
]

people.forEach(e => {
    const html = `<div class="people-item">
        <div class="poeple-img__wrapper">
            <img data-src="${e.img}" alt="${e.name}, ${e.position}" class="lazyload">
        </div>
        <div class="f-cust people-name section-eight__name">
            ${e.name}
        </div>
        <div class="f-smallest people-position section-eight__position">
            ${e.position}
        </div>
    </div>`;
    $('.section-eight__people-wrapper').append(html)
});

function myFunction() {
    myVar = setTimeout(showPage, 3000);
}

function showPage() {
    document.getElementById("page-loader").style.display = "none";
}

$(window).on('load', myFunction())

document.addEventListener("DOMContentLoaded", function () {
    var lazyVideos = [].slice.call(document.querySelectorAll("video.lazy"));

    if ("IntersectionObserver" in window) {
        var lazyVideoObserver = new IntersectionObserver(function (entries, observer) {
            entries.forEach(function (video) {
                if (video.isIntersecting) {
                    for (var source in video.target.children) {
                        var videoSource = video.target.children[source];
                        if (typeof videoSource.tagName === "string" && videoSource.tagName === "SOURCE") {
                            videoSource.src = videoSource.dataset.src;
                        }
                    }

                    video.target.load();
                    video.target.classList.remove("lazy");
                    lazyVideoObserver.unobserve(video.target);
                }
            });
        });

        lazyVideos.forEach(function (lazyVideo) {
            lazyVideoObserver.observe(lazyVideo);
        });
    }
});

$('label.uploadModal').click(function () {
    $(this).find('.fileToUploadModal').trigger('click');
})

$(".fileToUploadModal").change(function () {
    $(this).siblings('.valModal').text(this.value.replace(/C:\\fakepath\\/i, ''))
})

$('#button').click(function () {
    $("#fileToUpload").trigger('click');
})

$("#fileToUpload").change(function () {
    $('#val').text(this.value.replace(/C:\\fakepath\\/i, ''))
})