 // delete job
 $(document).on('click', '.btn-del-msg', (e) => {
    const id = $(e.target).closest('tr').data('id');
    const url = `${base_url}dashboard/deleteInbox/${id}`;
    Swal.fire({
        title: "Delete",
        text: "Are you sure want to delete this item?",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes',
        cancelButtonText: 'Cancel',
        reverseButtons: true
    }).then((result) => {
        if (result.isConfirmed) {
            window.location.href = url;
        }
    })
})