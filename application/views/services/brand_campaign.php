<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Brand Campaign | F4Elements</title>
    <!-- Bootstrap -->
    <link href="public/landing/css/bootstrap.min.css" rel="stylesheet">
    <style>
        html,
        body {
            width: 100%;
            height: 100%;
            margin: 0px;
            padding: 0px;
            overflow-x: hidden;
        }
    </style>
</head>

<body>
    <button type="button " class="close d-flex p-1" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true ">&times;</span>
    </button>
    <div class="modal-body text-center container">
        <div class="title__wrapper">
            <div class="f-cust title">
                Influencers campaign, digital activation
            </div>
            <div class="scroll">
                <div>
                    Scroll to explore
                </div>
                <a href="#test123" class="section-one__scroll-wrapper">
                    <img class="lazyload" data-src="<?= $this->public ?>/landing/assets/image/etc/arrowhead-down.png" width="100%" height="100%" />
                </a>
            </div>
        </div>
        <div class="text-center mb-4" id="test123">
            <img class="w-mob my-4 lazyload" data-src="<?= $this->public ?>/landing/assets/image/logo/fa-guide-logo-sajiku-714-acc 1.png" />
            <div class="">SAJIKU PREMIX</div>
        </div>
        <div class="text-left">
            <div class="f-smallest f-cust">OVERVIEW</div>
            <div class="f-smallest font-weight-normal">Sajiku as subsidiary of ajinomoto, provide product like sauces and instant premix coating mix and seasoning.
                </br></br>We work with them to create two interesting concept.
                </br></br>For Sajiku tepung Bakwan we create a digital quiz and inviting people
                to create their best version of bakwan with sajiku bakwan, the best winner can share their recipe and appear on an Instagram live session with several mega KOL such as Adelia Pasha
                </br></br>For Sajiku premix seasonings we create a Instagram
                filter game, where people can interact and collect the highest score and work with KOL to activate the quiz
            </div>
            <div class="row">
                <div class="col-6 col-lg-3">
                    <div class="f-cust f-smallest">Capability</div>
                    <div class="f-smallest font-weight-normal">Advertising</div>
                </div>
                <div class="col-6 col-lg-3">
                    <div class="f-cust f-smallest">Team</div>
                    <div class="f-smallest font-weight-normal">Interface Sunrise
                    </div>
                </div>
                <div class="col-6 col-lg-3">
                    <div class="f-cust f-smallest">Year</div>
                    <div class="f-smallest font-weight-normal">2020</div>
                </div>
                <div class="col-6 col-lg-3">
                    <div class="f-cust f-smallest">WEBSITE</div>
                    <a class="f-smallest font-weight-normal website-link" href="https://youtube.com/watch?time_continue=4&v=Is1ltAKGFTY&feature=emb_title" target="_blank">
                        Click here</a>
                </div>
            </div>
            <div class="gallery-wrapper">
                <div class="image-wrapper">
                    <img class="lazyload" class="mb-2" data-src="<?= $this->public ?>/landing/assets/image/image 87.png" />
                </div>
                <div class="image-wrapper">
                    <img class="lazyload" class="mb-2" data-src="<?= $this->public ?>/landing/assets/image/image 88.png" />
                </div>
                <div class="image-wrapper">
                    <img class="lazyload" class="mb-2" data-src="<?= $this->public ?>/landing/assets/image/image 89.png" />
                </div>
                <div class="image-wrapper">
                    <img class="lazyload" class="mb-2" data-src="<?= $this->public ?>/landing/assets/image/image 90.png" />
                </div>
            </div>
            <h5 class="mb-3 f-big f-cust2 text-center">Have a project in mind? Let's make it happen. together
            </h5>
            <div class="text-center">
                <a class="d-inline-block buttons w-max-content p-3 my-2 mb-4" href="#section-ten">
                    <div class="mb-0 f-smaller font-weight-bold">Let's have a chat</div>
                </a>
            </div>
        </div>
    </div>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="<?= $this->public ?>/landing/js/jquery-3.6.0.min.js "></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="<?= $this->public ?>/landing/js/bootstrap.min.js "></script>
    <!--Start of Tawk.to Script-->
    <script type="text/javascript">
        var Tawk_API = Tawk_API || {},
            Tawk_LoadStart = new Date();
        (function() {
            var s1 = document.createElement("script"),
                s0 = document.getElementsByTagName("script")[0];
            s1.async = true;
            s1.src = 'https://embed.tawk.to/60f6842ed6e7610a49ac1a22/1fb1f8lm3';
            s1.charset = 'UTF-8';
            s1.setAttribute('crossorigin', '*');
            s0.parentNode.insertBefore(s1, s0);
        })();
    </script>
    <!--End of Tawk.to Script-->
    <script src="<?= $this->public ?>/landing/js/lazysizes.min.js" async=""></script>

</body>

</html>