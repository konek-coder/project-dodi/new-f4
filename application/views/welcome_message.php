<!DOCTYPE html>
<html lang="en">

<head>
    <title>F4 Elements</title>
    <meta charset="utf-8">
    <meta name="title" content="F4Elements - Integrating Value Delivering Result">
    <meta name="description" content="We are into our elements and you’re welcomed to join">
    <meta name="keywords" content="business goal, grow net worth, integration">
    <meta name="robots" content="index, follow">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="language" content="English">
    <meta name="revisit-after" content="1 days">
    <meta name="author" content="F4Elements">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <!-- Open Graph / Facebook -->
    <meta property="og:type" content="website">
    <meta property="og:url" content="https://f4elements.com/">
    <meta property="og:title" content="F4Elements - Integrating Value Delivering Result">
    <meta property="og:description" content="We are into our elements and you’re welcomed to join">
    <meta property="og:image" content="https://f4elements.com/public/landing/assets/image/logo/f4%20shape%201.png">

    <!-- Twitter -->
    <meta property="twitter:card" content="summary_large_image">
    <meta property="twitter:url" content="https://f4elements.com/">
    <meta property="twitter:title" content="F4Elements - Integrating Value Delivering Result">
    <meta property="twitter:description" content="We are into our elements and you’re welcomed to join">
    <meta property="twitter:image" content="https://f4elements.com/public/landing/assets/image/logo/f4%20shape%201.png">

    <link rel="icon" type="image/png" href="public/landing/assets/image/logo/F4ELEMENTS.png" />

    <!-- Bootstrap -->
    <link href="public/landing/css/bootstrap.min.css" rel="stylesheet">
    <link href="public/landing/css/style.css?version=2021-09-12" rel="stylesheet">

    <link rel="preload" href="public/landing/assets/font/UnicaOne.ttf" as="font" crossorigin="anonymous">
    <link rel="preload" href="public/landing/assets/font/Dosis-Variable.ttf" as="font" crossorigin="anonymous">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@400;600;700&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Unica+One&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Rajdhani:wght@400;500;600&display=swap" rel="stylesheet">
    <script src="https://www.google.com/recaptcha/api.js" async defer></script>
</head>

<body>
    <div class="page-loader" id="page-loader">
        <div class="logo-wrapper">
        </div>
    </div>
    <div class="container container-f4 section-one" id="section-one">
        <a class="fixed-position-logo" href="#section-one">
        </a>

        <div class="fixed-position-menu p-0 border-0" data-toggle="modal" data-target="#menuModal">
            <div class="menu-box">
                <img class="lazyload d-block" data-src="./public/landing/assets/image/etc/hamburger.png" height="16px" width="32px" />
            </div>
        </div>

        <!-- section one -->
        <div class="row position-relative-xs p-4 section-one">
            <div class="col-12 col-lg-6 section-one__wrapper-left">
                <div class="section-one__wrapper-logo"></div>
                <div class="text-left w-75 section-one__title">
                    INTEGRATING VALUE DELIVERING RESULT
                </div>
                <div class="section-one__hr-wrapper">
                    <hr class="my-4 section-one__hr" />
                </div>
                <div class="f-smaller section-one__subtitle">These are our elements and you're welcomed to join </div>

                <!-- scroll mobile -->
                <div class="d-block d-sm-none">
                    <div class="text-center f-smallest section-one__scroll">
                        Scroll to explore
                    </div>

                    <div class="text-center ">
                        <div href="#" class="section-one__scroll-wrapper">
                            <img class="lazyload" data-src="public/landing/assets/image/etc/arrowhead-down.png" width="100%" height="100%" />
                        </div>
                    </div>
                </div>
            </div>

            <!-- image desktop -->
            <div class="section-one__image-wrapper-lg">
                <!-- <img class="lazyload" data-src="public/landing/assets/image/gif-desktop.gif" /> -->
                <div class="" style="width:422px;height:600px;overflow:hidden;">
                    <video class="lazy" width="426" height="605" autoplay muted loop playsinline>
                        <source data-src="<?= $this->public ?>/landing/assets/F4ELEMENTS-Logo-Desktop.mp4" type="video/mp4">
                        Your browser does not support the video tag.
                    </video>
                </div>
            </div>

            <!-- image mobile -->
            <div class="section-one__image-wrapper-mobile">
                <div class="position-absolute tr-pos" style="width:163px;height:234px;overflow:hidden;">
                    <!-- <img class="lazyload position-absolute tr-pos" data-src="public/landing/assets/image/gif-mobile.gif" height="237px" /> -->
                    <video class="lazy " width="auto" height="237" autoplay muted loop playsinline>
                        <source data-src="<?= $this->public ?>/landing/assets/F4ELEMENTS-Logo-Mobile.mp4" type="video/mp4">
                        Your browser does not support the video tag.
                    </video>
                </div>
            </div>
            <!-- scroll desktop -->
            <div class="col-lg-12 d-none d-sm-block">
                <div class="text-center f-small section-one__scroll">
                    Scroll to explore
                </div>

                <div class="text-center">
                    <div class="section-one__scroll-wrapper">
                        <img class="lazyload" data-src="public/landing/assets/image/etc/down-arrow-lg.svg" width="100%" height="100%" />
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- section two -->
    <div class="section-two bg-green-new" id="section-two">
        <div id="scroll" class="section-two__content-wrapper container-f4">
            <div class="section-two__title">
                <!-- We're here with you to achieve <span>your business goals</span> through the integration of <span>our elements</span> -->
                We're here with you to achieve your business goals through the integration of our elements
            </div>
            <div class="section-two__subtitle">
                <!-- Integrating the whole elements of below-the-line communication from getting the Insight of consumers and translate into <span>Strategies and Creative Ideas, connect to Communities and Stakeholders,</span> develop <span> Graphics and Animated Visualization,</span> to implement <span> Execution and Production.</span> -->
                <span> Integrating the whole elements of below-the-line communication from getting the </span> Insight of consumers <span>and translate into </span>Strategies and Creative Ideas,<span> connect to </span>Communities and Stakeholders,<span> develop </span> Graphics and Animated Visualization,<span> to implement </span> Execution and Production.
            </div>
            <div class="d-flex justify-content-center">
                <div class="w-max-content p-3 my-2 text-center section-two__btn have-chat">
                    <h6 class="mb-0 f-smaller font-weight-bold section-two__btn-text">Let's have a chat</h6>
                </div>
            </div>
        </div>
    </div>

    <!-- section three -->
    <div class="section-three bg-green-new">
        <div class="container-f4">
            <div class="section-three__title">
                We're going places
            </div>
            <div class="section-three__subtitle">
                Coverage of area where we can take your business to
            </div>
        </div>
        <div class="section-three__content-wrapper container-f4">
            <div class="left-item">
                <div class="section-three__map-wrapper">
                    <img class="lazyload" data-src="public/landing/assets/image/map.svg" alt="We're going places. Coverage of area where we can take your business to" srcset="" width="100%">
                </div>
            </div>
            <div class="right-item">
                <div class="section-three__icon-wrapper">
                    <img class="lazyload" data-src="public/landing/assets/image/logo-white.svg" alt="F4elements Logo" srcset="" width="80px">
                </div>
                <div class="section-three__countries-text">
                    <div>Hongkong & Macau</div>
                    <div>Singapore & Kuala Lumpur</div>
                    <div>Across Indonesia</div>
                </div>
            </div>
        </div>
    </div>

    <!-- section five -->
    <div class="section-five" id="section-five">
        <div class="section-five__content-wrapper">
            <div class="section-five__title">
                Our network can help you grow your net worth
            </div>
            <div class="section-five__network-wrapper">
                <!-- interface -->
                <div class="section-five__network-item">
                    <div class="section-five__network-item__image-wrapper interface">
                    </div>
                    <div class="section-five__network-item__title">
                        Brand Activation & Digital Products
                    </div>
                    <div class="section-five__network-item__summary">
                        The first company in the ecosystem. established since 2010, interface has been partnering up with clients from various categories, local and global brands, not only across Indonesia but also reaching up to Singapore, Malaysia, Hong Kong and Macau for brand’s marketing and sales programs.
                    </div>
                    <a class="section-five__find-out" href="https://www.instagram.com/interfacenation/?hl=en" target="_blank">
                        Find Out More
                    </a>
                    <!-- <div class="modal fade pr-0" id="modalNetwork" tabindex="-1" role="dialog" aria-labelledby="modalNetworkLabel" aria-hidden="true">
                        <div class="modal-dialog max-width-des modal-dialog-centered" role="document">
                            <div class="modal-content ">
                                <button type="button" class="close d-flex justify-content-end d-block d-sm-none mr-2" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true ">&times;</span>
                                </button>
                                <div class="modal-body text-center p-0 ">
                                    <div class="row ">
                                        <div class="col-12 col-lg-6 p-3 d-flex">
                                            <div class="row ">
                                                <div class="col-6 col-lg-12 order-1 order-lg-2">
                                                    <img class="mb-3 w-fit align-self-center lazyload" data-src='public/landing/assets/image/our_element/element1.png' />
                                                </div>
                                                <div class="col-6 col-lg-12 d-flex flex-column align-self-center order-2 order-lg-1">
                                                    <h6 class="f-big f-cust">
                                                        SUNRISE
                                                    </h6>
                                                    <h6 class="d-block d-lg-none f-smaller font-weight-normal">
                                                        Community & Event Management
                                                    </h6>
                                                </div>
                                                <div class="col-lg-12 d-none d-lg-block order-lg-3">
                                                    <h6 class="f-smaller font-weight-normal">
                                                        Community & Event Management
                                                    </h6>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-lg-6 bg-green py-4 px-5">
                                            <div class="row">
                                                <div class="col-12 mb-3">
                                                    <h6 class="f-smaller font-weight-bold text-white text-left mb-3">We are Thinkers, Designers, and Team Players.</h6>
                                                    <h6 class="f-smaller f-cust text-white text-left">Strong team performance is part of our professionalism in covering product needs, creating problem-solving design and completing your project.</h6>
                                                </div>
                                                <div class="col-6 mb-2">
                                                    <h6 class="f-bigger f-cust text-white">198</h6>
                                                    <h6 class="f-small text-white f-cust">EVENT CREATED</h6>
                                                </div>
                                                <div class="col-6 mb-2">
                                                    <h6 class="f-bigger f-cust text-white">1603</h6>
                                                    <h6 class="f-small text-white f-cust">BRAND ACTIVATION</h6>
                                                </div>
                                                <div class="col-6 mb-2">
                                                    <h6 class="f-bigger f-cust text-white">106</h6>
                                                    <h6 class="f-small text-white f-cust">HAPPY CLIENTS</h6>
                                                </div>
                                                <div class="col-6 mb-2">
                                                    <h6 class="f-bigger f-cust text-white">506</h6>
                                                    <h6 class="f-small text-white f-cust">SOCIAL CAMPAIGNS</h6>
                                                </div>
                                                <div class="col-12 mt-4 text-left">
                                                    <h6 class="f-smaller text-white font-weight-normal">Learn More
                                                        <span>
                                                            <img data-src="./public/landing/assets/image/etc/arrow right.png" class="lazyload" />
                                                        </span>
                                                    </h6>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> -->
                </div>

                <!-- giza -->
                <div class="section-five__network-item">
                    <div class="section-five__network-item__image-wrapper giza">
                    </div>
                    <div class="section-five__network-item__title">
                        Stage, Booth & Backrop, Interior, Other Construction
                    </div>
                    <div class="section-five__network-item__summary">
                        Intended as in-house booth and backdrop workshop for interface, now giza boasts more services including design and interior and partners up with production representative across Indonesia
                    </div>
                    <a href="https://www.instagram.com/giza_design_and_build/?hl=en" target="_blank" class="section-five__find-out">
                        Find Out More
                    </a>
                </div>

                <!-- store and display branding -->
                <div class="section-five__network-item">
                    <div class="section-five__network-item__image-wrapper element4branding">
                    </div>
                    <div class="section-five__network-item__title">
                        Store & Display Branding
                    </div>
                    <div class="section-five__network-item__summary">
                        The company is the spin-off from giza in 2019 to answer to an ever-increasing demand for POSM and product display production and installation. Like giza, element branding also have maintenance and installation people across Indonesia to serve clients and partners
                    </div>
                    <a href="https://www.instagram.com/elementbranding/?hl=en" target="_blank" class="section-five__find-out">
                        Find Out More
                    </a>
                </div>

                <!-- B2B printing -->
                <div class="section-five__network-item">
                    <div class="section-five__network-item__image-wrapper printworks">
                    </div>
                    <div class="section-five__network-item__title">
                        B2B and B2C printing (order shops & e-commerce)
                    </div>
                    <div class="section-five__network-item__summary">
                        Another spin-off company from giza, printworks does not only serve business partners on regular basis, but also individuals printing needs via mortar-and-brick as well as online shop
                    </div>
                    <a href="https://www.instagram.com/printworks_id/?hl=en" target="_blank" class="section-five__find-out">
                        Find Out More
                    </a>
                </div>

                <!-- Community & Event Management -->
                <div class="section-five__network-item">
                    <div class="section-five__network-item__image-wrapper sunrise">
                    </div>
                    <div class="section-five__network-item__title">
                        Community & Event Management
                    </div>
                    <div class="section-five__network-item__summary">
                        The company is designed to work together with all type of communities and link them to relevant stakeholders so the communities can contribute their value to public
                    </div>
                    <a href="https://www.instagram.com/onthesunrise.id/?utm_medium=copy_link" target="_blank" class="section-five__find-out">
                        Find Out More
                    </a>
                </div>

                <!-- Virtual Event Management -->
                <div class="section-five__network-item">
                    <div class="section-five__network-item__image-wrapper studio">
                    </div>
                    <div class="section-five__network-item__title">
                        Virtual Event Organizer
                    </div>
                    <div class="section-five__network-item__summary">
                        Born out of necessity, in 2020, studio terogong’s main objective is to serve customers end-to-end. Not only provide venue and hardware tech equipments, studio terogong also offers creative ideas and design, execution, and participation management & database
                    </div>
                    <div class="section-five__find-out">
                        Find Out More
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- section six -->
    <div class="section-six container-f4" id="section-six">
        <div class="section-six__content-wrapper">
            <div class="section-six__title">
                You’ll be in good company
            </div>
            <div class="section-six__subtitle">
                Leading brands we’ve partnered with
            </div>
            <div class="section-six__wrapper-brand">
                <div class="section-six__wrapper-item active" data-target="dairy">
                    Dairy (+ Scientific)
                </div>
                <div class="section-six__wrapper-item" data-target="oil">
                    Oil & Gas
                </div>
                <div class="section-six__wrapper-item" data-target="telco">
                    Telco
                </div>
                <div class="section-six__wrapper-item" data-target="otomotif">
                    Automotive
                </div>
                <div class="section-six__wrapper-item" data-target="tobacco">
                    Tobacco
                </div>
                <div class="section-six__wrapper-item" data-target="beverages">
                    Beverages
                </div>
                <div class="section-six__wrapper-item" data-target="finance">
                    Finance
                </div>
                <div class="section-six__wrapper-item" data-target="fashion">
                    Fashion
                </div>
                <div class="section-six__wrapper-item" data-target="electronic">
                    Electronic & Technology
                </div>
                <div class="section-six__wrapper-item" data-target="personal">
                    Personal & Health Care
                </div>
                <div class="section-six__wrapper-item" data-target="government">
                    Goverment & Corporate
                </div>
                <div class="section-six__wrapper-item" data-target="initiative">
                    Initiative/Pro-Bono
                </div>
            </div>
            <div class="section-six__wrapper-product">
                <div class="section-six__product-item dairy">
                    <div class="product-item__wrapper">
                        <img class="lazyload" data-src="./public/landing/assets/image/our_clients/dairy-1.png" alt="F4elements Brand Partner" width="auto">
                    </div>
                    <div class="product-item__wrapper">
                        <img class="lazyload" data-src="./public/landing/assets/image/our_clients/dairy-2.png" alt="F4elements Brand Partner" width="auto">
                    </div>
                    <div class="product-item__wrapper">
                        <img class="lazyload" data-src="./public/landing/assets/image/our_clients/dairy-3.png" alt="F4elements Brand Partner" width="auto">
                    </div>
                    <div class="product-item__wrapper">
                        <img class="lazyload" data-src="./public/landing/assets/image/our_clients/dairy-4.png" alt="F4elements Brand Partner" width="auto">
                    </div>
                    <div class="product-item__wrapper">
                        <img class="lazyload" data-src="./public/landing/assets/image/our_clients/dairy-5.png" alt="F4elements Brand Partner" width="auto">
                    </div>
                    <div class="product-item__wrapper">
                        <img class="lazyload" data-src="./public/landing/assets/image/our_clients/dairy-6.png" alt="F4elements Brand Partner" width="auto">
                    </div>
                    <div class="product-item__wrapper">
                        <img class="lazyload" data-src="./public/landing/assets/image/our_clients/dairy-7.png" alt="F4elements Brand Partner" width="auto">
                    </div>
                    <div class="product-item__wrapper">
                        <img class="lazyload" data-src="./public/landing/assets/image/our_clients/dairy-8.png" alt="F4elements Brand Partner" width="auto">
                    </div>
                    <div class="product-item__wrapper">
                        <img class="lazyload" data-src="./public/landing/assets/image/our_clients/dairy-9.png" alt="F4elements Brand Partner" width="auto">
                    </div>
                    <div class="product-item__wrapper">
                        <img class="lazyload" data-src="./public/landing/assets/image/our_clients/dairy-10.png" alt="F4elements Brand Partner" width="auto">
                    </div>
                    <div class="product-item__wrapper">
                        <img class="lazyload" data-src="./public/landing/assets/image/our_clients/dairy-11.png" alt="F4elements Brand Partner" width="auto">
                    </div>
                    <div class="product-item__wrapper">
                        <img class="lazyload" data-src="./public/landing/assets/image/our_clients/dairy-12.png" alt="F4elements Brand Partner" width="auto">
                    </div>
                    <div class="product-item__wrapper">
                        <img class="lazyload" data-src="./public/landing/assets/image/our_clients/dairy-13.png" alt="F4elements Brand Partner" width="auto">
                    </div>
                    <div class="product-item__wrapper">
                        <img class="lazyload" data-src="./public/landing/assets/image/our_clients/dairy-14.png" alt="F4elements Brand Partner" width="auto">
                    </div>
                    <div class="product-item__wrapper">
                        <img class="lazyload" data-src="./public/landing/assets/image/our_clients/dairy-15.png" alt="F4elements Brand Partner" width="auto">
                    </div>
                </div>
                <div class="section-six__product-item oil" style="display: none;">
                    <div class="product-item__wrapper">
                        <img class="lazyload" data-src="./public/landing/assets/image/our_clients/oil-1.png" alt="F4elements Brand Partner" width="auto">
                    </div>
                    <div class="product-item__wrapper">
                        <img class="lazyload" data-src="./public/landing/assets/image/our_clients/oil-2.png" alt="F4elements Brand Partner" width="auto">
                    </div>
                </div>
                <div class="section-six__product-item telco" style="display: none;">
                    <div class="product-item__wrapper">
                        <img class="lazyload" data-src="./public/landing/assets/image/our_clients/telco-1.png" alt="F4elements Brand Partner" width="auto">
                    </div>
                    <div class="product-item__wrapper">
                        <img class="lazyload" data-src="./public/landing/assets/image/our_clients/telco-2.png" alt="F4elements Brand Partner" width="auto">
                    </div>
                    <div class="product-item__wrapper">
                        <img class="lazyload" data-src="./public/landing/assets/image/our_clients/telco-3.png" alt="F4elements Brand Partner" width="auto">
                    </div>
                    <div class="product-item__wrapper">
                        <img class="lazyload" data-src="./public/landing/assets/image/our_clients/telco-4.png" alt="F4elements Brand Partner" width="auto">
                    </div>
                    <div class="product-item__wrapper">
                        <img class="lazyload" data-src="./public/landing/assets/image/our_clients/telco-5.png" alt="F4elements Brand Partner" width="auto">
                    </div>
                </div>
                <div class="section-six__product-item otomotif" style="display: none;">
                    <div class="product-item__wrapper">
                        <img class="lazyload" data-src="./public/landing/assets/image/our_clients/oto-1.png" alt="F4elements Brand Partner" width="auto">
                    </div>
                    <div class="product-item__wrapper">
                        <img class="lazyload" data-src="./public/landing/assets/image/our_clients/oto-2.png" alt="F4elements Brand Partner" width="auto">
                    </div>
                    <div class="product-item__wrapper">
                        <img class="lazyload" data-src="./public/landing/assets/image/our_clients/oto-3.png" alt="F4elements Brand Partner" width="auto">
                    </div>
                    <div class="product-item__wrapper">
                        <img class="lazyload" data-src="./public/landing/assets/image/our_clients/oto-4.png" alt="F4elements Brand Partner" width="auto">
                    </div>
                    <div class="product-item__wrapper">
                        <img class="lazyload" data-src="./public/landing/assets/image/our_clients/oto-5.png" alt="F4elements Brand Partner" width="auto">
                    </div>
                    <div class="product-item__wrapper">
                        <img class="lazyload" data-src="./public/landing/assets/image/our_clients/oto-6.png" alt="F4elements Brand Partner" width="auto">
                    </div>
                    <div class="product-item__wrapper">
                        <img class="lazyload" data-src="./public/landing/assets/image/our_clients/oto-7.png" alt="F4elements Brand Partner" width="auto">
                    </div>
                    <div class="product-item__wrapper">
                        <img class="lazyload" data-src="./public/landing/assets/image/our_clients/oto-8.png" alt="F4elements Brand Partner" width="auto">
                    </div>
                    <div class="product-item__wrapper">
                        <img class="lazyload" data-src="./public/landing/assets/image/our_clients/oto-9.png" alt="F4elements Brand Partner" width="auto">
                    </div>
                    <div class="product-item__wrapper">
                        <img class="lazyload" data-src="./public/landing/assets/image/our_clients/oto-10.png" alt="F4elements Brand Partner" width="auto">
                    </div>
                    <div class="product-item__wrapper">
                        <img class="lazyload" data-src="./public/landing/assets/image/our_clients/oto-11.png" alt="F4elements Brand Partner" width="auto">
                    </div>
                </div>
                <div class="section-six__product-item tobacco" style="display: none;">
                    <div class="product-item__wrapper">
                        <img class="lazyload" data-src="./public/landing/assets/image/our_clients/tobacco-1.png" alt="F4elements Brand Partner" width="auto">
                    </div>
                    <div class="product-item__wrapper">
                        <img class="lazyload" data-src="./public/landing/assets/image/our_clients/tobacco-2.png" alt="F4elements Brand Partner" width="auto">
                    </div>
                    <div class="product-item__wrapper">
                        <img class="lazyload" data-src="./public/landing/assets/image/our_clients/tobacco-3.png" alt="F4elements Brand Partner" width="auto">
                    </div>
                </div>
                <div class="section-six__product-item beverages" style="display: none;">
                    <div class="product-item__wrapper">
                        <img class="lazyload" data-src="./public/landing/assets/image/our_clients/beverages-1.png" alt="F4elements Brand Partner" width="auto">
                    </div>
                    <div class="product-item__wrapper">
                        <img class="lazyload" data-src="./public/landing/assets/image/our_clients/beverages-2.png" alt="F4elements Brand Partner" width="auto">
                    </div>
                    <div class="product-item__wrapper">
                        <img class="lazyload" data-src="./public/landing/assets/image/our_clients/beverages-3.png" alt="F4elements Brand Partner" width="auto">
                    </div>
                    <div class="product-item__wrapper">
                        <img class="lazyload" data-src="./public/landing/assets/image/our_clients/beverages-4.png" alt="F4elements Brand Partner" width="auto">
                    </div>
                    <div class="product-item__wrapper">
                        <img class="lazyload" data-src="./public/landing/assets/image/our_clients/beverages-5.png" alt="F4elements Brand Partner" width="auto">
                    </div>
                    <div class="product-item__wrapper">
                        <img class="lazyload" data-src="./public/landing/assets/image/our_clients/beverages-6.png" alt="F4elements Brand Partner" width="auto">
                    </div>
                    <div class="product-item__wrapper">
                        <img class="lazyload" data-src="./public/landing/assets/image/our_clients/beverages-7.png" alt="F4elements Brand Partner" width="auto">
                    </div>
                </div>
                <div class="section-six__product-item finance" style="display: none;">
                    <div class="product-item__wrapper">
                        <img class="lazyload" data-src="./public/landing/assets/image/our_clients/finance-1.png" alt="F4elements Brand Partner" width="auto">
                    </div>
                    <div class="product-item__wrapper">
                        <img class="lazyload" data-src="./public/landing/assets/image/our_clients/finance-2.png" alt="F4elements Brand Partner" width="auto">
                    </div>
                    <div class="product-item__wrapper">
                        <img class="lazyload" data-src="./public/landing/assets/image/our_clients/finance-3.png" alt="F4elements Brand Partner" width="auto">
                    </div>
                    <div class="product-item__wrapper">
                        <img class="lazyload" data-src="./public/landing/assets/image/our_clients/finance-4.png" alt="F4elements Brand Partner" width="auto">
                    </div>
                    <div class="product-item__wrapper">
                        <img class="lazyload" data-src="./public/landing/assets/image/our_clients/finance-5.png" alt="F4elements Brand Partner" width="auto">
                    </div>
                    <div class="product-item__wrapper">
                        <img class="lazyload" data-src="./public/landing/assets/image/our_clients/finance-6.png" alt="F4elements Brand Partner" width="auto">
                    </div>
                </div>
                <div class="section-six__product-item fashion" style="display: none;">
                    <div class="product-item__wrapper">
                        <img class="lazyload" data-src="./public/landing/assets/image/our_clients/fashion-1.png" alt="F4elements Brand Partner" width="auto">
                    </div>
                    <div class="product-item__wrapper">
                        <img class="lazyload" data-src="./public/landing/assets/image/our_clients/fashion-2.png" alt="F4elements Brand Partner" width="auto">
                    </div>
                </div>
                <div class="section-six__product-item electronic" style="display: none;">
                    <div class="product-item__wrapper">
                        <img class="lazyload" data-src="./public/landing/assets/image/our_clients/electronic-1.png" alt="F4elements Brand Partner" width="auto">
                    </div>
                    <div class="product-item__wrapper">
                        <img class="lazyload" data-src="./public/landing/assets/image/our_clients/electronic-2.png" alt="F4elements Brand Partner" width="auto">
                    </div>
                    <div class="product-item__wrapper">
                        <img class="lazyload" data-src="./public/landing/assets/image/our_clients/electronic-3.png" alt="F4elements Brand Partner" width="auto">
                    </div>
                    <div class="product-item__wrapper">
                        <img class="lazyload" data-src="./public/landing/assets/image/our_clients/electronic-4.png" alt="F4elements Brand Partner" width="auto">
                    </div>
                    <div class="product-item__wrapper">
                        <img class="lazyload" data-src="./public/landing/assets/image/our_clients/electronic-5.png" alt="F4elements Brand Partner" width="auto">
                    </div>
                    <div class="product-item__wrapper">
                        <img class="lazyload" data-src="./public/landing/assets/image/our_clients/electronic-6.png" alt="F4elements Brand Partner" width="auto">
                    </div>
                    <div class="product-item__wrapper">
                        <img class="lazyload" data-src="./public/landing/assets/image/our_clients/electronic-7.png" alt="F4elements Brand Partner" width="auto">
                    </div>
                    <div class="product-item__wrapper">
                        <img class="lazyload" data-src="./public/landing/assets/image/our_clients/electronic-8.png" alt="F4elements Brand Partner" width="auto">
                    </div>
                </div>
                <div class="section-six__product-item personal" style="display: none;">
                    <div class="product-item__wrapper">
                        <img class="lazyload" data-src="./public/landing/assets/image/our_clients/personal-1.png" alt="F4elements Brand Partner" width="auto">
                    </div>
                    <div class="product-item__wrapper">
                        <img class="lazyload" data-src="./public/landing/assets/image/our_clients/personal-2.png" alt="F4elements Brand Partner" width="auto">
                    </div>
                    <div class="product-item__wrapper">
                        <img class="lazyload" data-src="./public/landing/assets/image/our_clients/personal-3.png" alt="F4elements Brand Partner" width="auto">
                    </div>
                    <div class="product-item__wrapper">
                        <img class="lazyload" data-src="./public/landing/assets/image/our_clients/personal-4.png" alt="F4elements Brand Partner" width="auto">
                    </div>
                    <div class="product-item__wrapper">
                        <img class="lazyload" data-src="./public/landing/assets/image/our_clients/personal-5.png" alt="F4elements Brand Partner" width="auto">
                    </div>
                    <div class="product-item__wrapper">
                        <img class="lazyload" data-src="./public/landing/assets/image/our_clients/personal-6.png" alt="F4elements Brand Partner" width="auto">
                    </div>
                    <div class="product-item__wrapper">
                        <img class="lazyload" data-src="./public/landing/assets/image/our_clients/personal-7.png" alt="F4elements Brand Partner" width="auto">
                    </div>
                    <div class="product-item__wrapper">
                        <img class="lazyload" data-src="./public/landing/assets/image/our_clients/personal-8.png" alt="F4elements Brand Partner" width="auto">
                    </div>
                </div>
                <div class="section-six__product-item government" style="display: none;">
                    <div class="product-item__wrapper">
                        <img class="lazyload" data-src="./public/landing/assets/image/our_clients/gov-1.png" alt="F4elements Brand Partner" width="auto">
                    </div>
                    <div class="product-item__wrapper">
                        <img class="lazyload" data-src="./public/landing/assets/image/our_clients/gov-2.png" alt="F4elements Brand Partner" width="auto">
                    </div>
                    <div class="product-item__wrapper">
                        <img class="lazyload" data-src="./public/landing/assets/image/our_clients/gov-3.png" alt="F4elements Brand Partner" width="auto">
                    </div>
                    <div class="product-item__wrapper">
                        <img class="lazyload" data-src="./public/landing/assets/image/our_clients/gov-4.png" alt="F4elements Brand Partner" width="auto">
                    </div>
                    <div class="product-item__wrapper">
                        <img class="lazyload" data-src="./public/landing/assets/image/our_clients/gov-5.png" alt="F4elements Brand Partner" width="auto">
                    </div>
                    <div class="product-item__wrapper">
                        <img class="lazyload" data-src="./public/landing/assets/image/our_clients/gov-6.png" alt="F4elements Brand Partner" width="auto">
                    </div>
                    <div class="product-item__wrapper">
                        <img class="lazyload" data-src="./public/landing/assets/image/our_clients/gov-7.png" alt="F4elements Brand Partner" width="auto">
                    </div>
                    <div class="product-item__wrapper">
                        <img class="lazyload" data-src="./public/landing/assets/image/our_clients/gov-8.png" alt="F4elements Brand Partner" width="auto">
                    </div>
                    <div class="product-item__wrapper">
                        <img class="lazyload" data-src="./public/landing/assets/image/our_clients/gov-9.png" alt="F4elements Brand Partner" width="auto">
                    </div>
                    <div class="product-item__wrapper">
                        <img class="lazyload" data-src="./public/landing/assets/image/our_clients/gov-10.png" alt="F4elements Brand Partner" width="auto">
                    </div>
                    <div class="product-item__wrapper">
                        <img class="lazyload" data-src="./public/landing/assets/image/our_clients/gov-11.png" alt="F4elements Brand Partner" width="auto">
                    </div>
                    <div class="product-item__wrapper">
                        <img class="lazyload" data-src="./public/landing/assets/image/our_clients/gov-12.png" alt="F4elements Brand Partner" width="auto">
                    </div>
                    <div class="product-item__wrapper">
                        <img class="lazyload" data-src="./public/landing/assets/image/our_clients/gov-13.png" alt="F4elements Brand Partner" width="auto">
                    </div>
                    <div class="product-item__wrapper">
                        <img class="lazyload" data-src="./public/landing/assets/image/our_clients/gov-14.png" alt="F4elements Brand Partner" width="auto">
                    </div>
                </div>
                <div class="section-six__product-item initiative" style="display: none;">
                    <div class="product-item__wrapper">
                        <img class="lazyload" data-src="./public/landing/assets/image/our_clients/initiative-1.png" alt="F4elements Brand Partner" width="auto">
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container-f4">
        <!-- section seven -->
        <div class="section-seven__container" id="section-seven">
            <div class="section-seven">
                <div class="section-seven__title">
                    F4Elements offer best-in-class solutions offline, online, and everything in between
                </div>
                <div class="section-seven__item-right">
                    <div class="section-seven__item-graph">
                        <div class="section-seven__wrapper-graph">
                            <div class="graph-wrapper">
                                <div class="graph">
                                    <video class="lazy" width="300" height="300" autoplay muted loop playsinline>
                                        <source data-src="<?= $this->public ?>/landing/assets/F4ELEMENTS-Services-Mobile.mp4" type="video/mp4">
                                        Your browser does not support the video tag.
                                    </video>
                                    <!-- <img data-src="public/landing/assets/image/etc/Service_White.gif" alt="F4Elements offer best-in-class solutions offline, online, and everything in between" class="lazyload" width="100%" height="100%"> -->
                                    <?php
                                    foreach ($services as $key => $item) {
                                        if ($item->is_published == 1) { ?>
                                            <div class="graph-rect" id="<?= $item->code ?>" data-toggle="modal" data-target="#modal-service-<?= $item->id ?>"></div>
                                    <?php }
                                    } ?>
                                </div>
                                <?php
                                foreach ($services as $key => $item) {
                                    if ($item->is_published == 1) { ?>
                                        <div class="modal fade service modal-service-<?= $item->id ?>" id="modal-service-<?= $item->id ?>" tabindex="-1" role="dialog" aria-labelledby="modalInfLabel" aria-hidden="true">
                                            <div class="modal-dialog d-flex " role="document ">
                                                <div class="modal-content overflow-auto">
                                                    <div class="top-section">
                                                        <div class="close-wrapper" data-dismiss="modal">
                                                            <img class="lazyload" data-src="./public/landing/assets/image/cross-green.svg" alt="" width="20px">
                                                        </div>
                                                        <!-- <div class="title__wrapper">
                                                            <div class="f-cust title">
                                                                <?= $item->popup_title ?>
                                                            </div>
                                                        </div>
                                                        <div class="scroll">
                                                            <div>
                                                                Scroll to explore
                                                            </div>
                                                            <a data-href="#scrollBrand" data-target-modal="modalInf" class="section-one__scroll-wrapper scrollToBrand" data-modal="test">
                                                                <img class="lazyload" data-src="public/landing/assets/image/etc/arrowhead-down.svg" width="100%" height="100%" />
                                                            </a>
                                                        </div> -->
                                                    </div>
                                                    <div class="modal-body text-center container">
                                                        <div class="text-center" id="scrollBrand">
                                                            <img class="w-mob my-4 lazyload img-brand" data-src="upload/service/<?= $item->id; ?>/<?= $item->logo ?>" />
                                                            <div class="brand-title"><?= $item->brand_name ?></div>
                                                            <div class="hr"></div>
                                                        </div>
                                                        <div class="text-left">
                                                            <div class="info-wrapper">
                                                                <div class="f-smallest f-cust item-title">OVERVIEW</div>
                                                                <div class="f-smallest font-weight-normal item-body"><?= $item->overview ?></div>
                                                                <div class="row">
                                                                    <div class="col-6 col-lg-3">
                                                                        <div class="f-cust f-smallest item-title m-0">SERVICES</div>
                                                                        <div class="f-smallest font-weight-normal item-body"><?= $item->capability ?></div>
                                                                    </div>
                                                                    <div class="col-6 col-lg-3">
                                                                        <div class="f-cust f-smallest item-title m-0">ELEMENTS</div>
                                                                        <div class="f-smallest font-weight-normal item-body"><?= $item->team ?>
                                                                        </div>
                                                                    </div>
                                                                    <!-- <div class="col-6 col-lg-3">
                                                                        <div class="f-cust f-smallest item-title m-0">Year</div>
                                                                        <div class="f-smallest font-weight-normal item-body"><?= $item->year ?></div>
                                                                    </div>
                                                                    <div class="col-6 col-lg-3">
                                                                        <div class="f-cust f-smallest item-title m-0">WEBSITE</div>
                                                                        <a class="f-smallest font-weight-normal website-link item-body" href="<?= $item->website ?>" target="_blank">
                                                                            Click here</a>
                                                                    </div> -->
                                                                </div>
                                                            </div>
                                                            <div class="gallery-wrapper row">
                                                                <?php
                                                                $images = json_decode($item->images);
                                                                foreach ($images as $key => $image) {
                                                                    list($width, $height, $type, $attr) = getimagesize("upload/service/" . $item->id . "/" . $image);
                                                                    if ($width >= 1000) {
                                                                        echo '<div class="col-sm-12 picture-item">';
                                                                        echo '<img class="lazyload" data-src="upload/service/' . $item->id . '/' . $image . '" width="100%" />';
                                                                        echo '</div>';
                                                                    } elseif ($width >= 500) {
                                                                        echo '<div class="col-sm-6 picture-item">';
                                                                        echo '<img class="lazyload" data-src="upload/service/' . $item->id . '/' . $image . '" width="100%" />';
                                                                        echo '</div>';
                                                                    } else {
                                                                        echo '<div class="col-sm-6 col-md-4 col-lg-3 picture-item">';
                                                                        echo '<img class="lazyload" data-src="upload/service/' . $item->id . '/' . $image . '" width="100%" />';
                                                                        echo '</div>';
                                                                    }
                                                                }
                                                                ?>
                                                            </div>
                                                            <div class="f-cust2 section-seven__make-it">Have a project in mind? Let's make it happen. together
                                                            </div>
                                                            <div class="text-center">
                                                                <a class="d-inline-block buttons w-max-content p-3 internal-link-modal" href="#section-ten">
                                                                    <div class="mb-0 f-smaller font-weight-bold">Let's have a chat</div>
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="footer">
                                                        <div class="border-social">
                                                            <!-- <div class="d-flex justify-content-between section-ten__socials-wrapper">
                                                            <a href="#" class="section-ten__social-wrapper">
                                                                <img class="lazyload" data-src="public/landing/assets/image/social_media/fb-green.svg" alt="" srcset="">
                                                            </a>
                                                            <a href="#" class="section-ten__social-wrapper">
                                                                <img class="lazyload" data-src="public/landing/assets/image/social_media/tw-green.svg" alt="" srcset="">
                                                            </a>
                                                            <a href="#" class="section-ten__social-wrapper">
                                                                <img class="lazyload" data-src="public/landing/assets/image/social_media/ig-green.svg" alt="" srcset="">
                                                            </a>
                                                            <a href="#" class="section-ten__social-wrapper">
                                                                <img class="lazyload" data-src="public/landing/assets/image/social_media/yt-green.svg" alt="" srcset="">
                                                            </a>
                                                            <a href="#" class="section-ten__social-wrapper">
                                                                <img class="lazyload" data-src="public/landing/assets/image/social_media/li-green.svg" alt="" srcset="">
                                                            </a>
                                                        </div> -->
                                                        </div>
                                                        <div class="copyright">
                                                            Ⓒ 2021 F4ELEMENTS, ALL RIGHTS RESERVED.
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                <?php }
                                } ?>
                            </div>
                        </div>
                    </div>
                    <div class="section-seven__item-text">
                        <div>
                            What we do for brands is always about increasing equity, increasing sales, or both. </br> </br> The services that F4Elements companies offer are designed to result in one fundamental goal: <span>business growth.</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- section eight -->
        <div class="section-eight" id="section-eight">
            <div class="section-eight__title">The People's People</div>
            <div class="section-eight__line">
                <div class="section-eight__line-green"></div>
            </div>
            <div class="section-eight__people-container">
                <div class="section-eight__people-wrapper">
                    <div class="people-item">
                        <div class="poeple-img__wrapper">
                            <img class="lazyload" data-src="./public/landing/assets/image/people/people1.png " alt="MURIO, Director, F4Elements">
                        </div>
                        <div class="section-eight__name">
                            MURIO
                        </div>
                        <div class="section-eight__position">
                            Director, F4Elements
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- section nine -->
        <div class="section-nine" id="section-nine">
            <div class="">
                <div class="section-nine__title">What's Your Element?</div>
            </div>
            <div class="d-flex flex-wrap justify-content-between">
                <?php
                foreach ($careers as $key => $item) { ?>
                    <div data-toggle="modal" data-target="#career<?= $key ?>" class="section-nine__career-item bg-green d-flex justify-content-between align-items-center section-nine__career-item">
                        <div>
                            <div class="section-nine__job-position"><?= $item->title; ?></div>
                            <div class="section-nine__see-details">
                                Drop your resume here
                            </div>
                        </div>
                        <div>
                            <img class="lazyload" data-src="./public/landing/assets/image/etc/arrow.png " />
                        </div>
                    </div>
                    <!-- modal -->
                    <div class="modal modal-career overflow-hidden pr-0" id="career<?= $key ?>" tabindex="-1" role="dialog" aria-labelledby="career<?= $key ?>Label" aria-hidden="true">
                        <div class="modal-dialog d-flex h-fill" role="document">
                            <div class="modal-content career h-80 position-absolute">
                                <div class="modal-header bg-green text-center">
                                    <img src="./public/landing/assets/image/logo/test1.png" alt="">
                                    <div class="f-cust text-white m-auto career-title"><?= $item->title; ?></div>
                                    <button type="button" class="close d-flex justify-content-end p-3" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true "><img class="lazyload" data-src="public/landing/assets/image/close.png" /></span>
                                    </button>
                                </div>
                                <div class="modal-body career overflow-auto">
                                    <div class="">
                                        <h5 class="f-cust mb-4 info-career-code ">Job Code: <?= $item->job_code; ?></h5>
                                        <h6 class="f-smallest mb-2 info-career-title">Job Description:</h6>
                                        <ul class="mb-3 ">
                                            <?php
                                            $descriptions = json_decode($item->job_desc);
                                            if (is_array($descriptions))
                                                foreach ($descriptions as $key => $description) { ?>
                                                <li class="f-smallest info-career-item"><?= $description ?></li>
                                            <?php }
                                            else echo '<p class="f-smallest info-career-item">' . $item->job_desc . '</p>';
                                            ?>
                                        </ul>
                                        <h6 class="f-smallest mb-2 info-career-title">Requirements:</h6>
                                        <ul class="mb-3 ">
                                            <?php
                                            $requirements = json_decode($item->requirements);
                                            if (is_array($requirements))
                                                foreach ($requirements as $key => $requirement) { ?>
                                                <li class="f-smallest info-career-item"><?= $requirement ?></li>
                                            <?php }
                                            else echo '<p class="f-smallest ">' . $item->requirements . '</p>';
                                            ?>
                                        </ul>
                                        <div class="mb-3 ">
                                            <h6 class="f-smallest font-weight-bold info-career-title">Job Location</h6>
                                            <h6 class="f-smallest font-weight-normal info-career-item"><?= $item->job_location; ?></h6>
                                        </div>
                                        <div class="mb-3 ">
                                            <h6 class="f-smallest font-weight-bold info-career-title">Salary</h6>
                                            <h6 class="f-smallest font-weight-normal info-career-item"><?= $item->salary; ?></h6>
                                        </div>
                                        <h6 class="f-smallest mb-3 font-weight-normal info-career-item">
                                            <?= $item->summary; ?>
                                        </h6>
                                        <h6 class="f-smallest mb-4 font-weight-normal info-career-item">
                                            Email david@f4elements.com or you can easily fill the form below.
                                        </h6>
                                        <form id="formCareer<?= $item->id ?>" action="<?= base_url('/welcome/storeMsg') ?>" method="post" enctype="multipart/form-data">
                                            <div class="career-form__wrapper form-group mb-5 ">
                                                <div class="career-form__field-item">
                                                    <label for="exampleInputName1" class="career-form-label">YOUR NAME<span>*</span></label>
                                                    <input type="text" name="name" class="color-placeholder form-control form-cust mb-5 pl-0 " id="exampleInputName1 " aria-describedby="nameHelp " placeholder="" required>
                                                </div>
                                                <div class="career-form__field-item">
                                                    <label for="exampleInputEmail1" class="career-form-label">YOUR EMAIL<span>*</span></label>
                                                    <input type="text" name="email" class="color-placeholder form-control form-cust mb-5 pl-0" id="exampleInputEmail1 " aria-describedby="emailHelp " placeholder="" required>
                                                </div>
                                                <div class="career-form__field-item">
                                                    <label for="exampleInputSubject1" class="career-form-label">YOUR SUBJECT<span>*</span></label>
                                                    <input type="text" name="subject" class="color-placeholder form-control form-cust mb-4 pl-0" id="exampleInputSubject1 " aria-describedby="subjectHelp " placeholder="" value="<?= $item->job_code; ?>" required title="Please fill with the job code">
                                                </div>
                                                <div class="career-form__field-item">
                                                    <label for="fileToUploadModal<?= $item->id ?>" class="custom-file-input-f4 uploadModal" style="position:relative">
                                                        <input name="fileToUpload" type="file" class="fileToUploadModal" id="fileToUploadModal<?= $item->id ?>"><span id='valModal' class="valModal" style="color:#212936"></span>
                                                    </label>
                                                    <label class="less-than">*less than 1.5 Mb</label>
                                                </div>
                                                <div class="career-form__field-item">
                                                    <div class="g-recaptcha" data-sitekey="6LfoxEccAAAAAJ5bDVgtQAQ4Wrd008u5hZDgaXyq"></div>
                                                </div>

                                            </div>
                                            <div class="text-center">
                                                <input type="submit" value="SEND MESSAGES" class="btn px-3 border-radius font-weight-bold submit-career">
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php }
                ?>
            </div>
        </div>
    </div>

    <!-- section ten -->
    <div class="bg-green section-ten" id="section-ten">
        <div class="w-100 container-f4">
            <div class="text-white section-ten__title">Have a project in mind? Let's make it happen, together.</div>
            <div class="row ">
                <div class="col-12 col-lg-6 ">
                    <form method="post" action="<?= base_url('/welcome/storeMsg/') ?>" enctype="multipart/form-data">
                        <div class=" form-group mb-5 ">
                            <div class="row ">
                                <div class="col-12 col-lg-6 ">
                                    <label for="" class="career-form-label">YOUR NAME<span>*</span></label>
                                    <input name="name" type="text" class="color-placeholder form-control form-cust pl-0 section-ten__input-text" id="exampleInputName1 " aria-describedby="nameHelp " placeholder="" required>
                                </div>
                                <div class="col-12 col-lg-6 ">
                                    <label for="" class="career-form-label">YOUR EMAIL<span>*</span></label>
                                    <input name="email" type="email" class="color-placeholder form-control form-cust pl-0 section-ten__input-text" id="exampleInputEmail1 " aria-describedby="emailHelp " placeholder="" required>
                                </div>
                                <div class="col-12 col-lg-6 ">
                                    <label for="" class="career-form-label">YOUR SUBJECT<span>*</span></label>
                                    <input name="subject" type="text" class="color-placeholder form-control form-cust pl-0 section-ten__input-text" id="exampleInputSubject1 " aria-describedby="subjectHelp " placeholder="" required>
                                </div>
                                <div class="col-12 col-lg-6 ">
                                    <label for="fileToUpload" class="custom-file-input-f4" style="position:relative">
                                        <input name="fileToUpload" type="file" class="" id="fileToUpload"><span id='val' class="text-white"></span>
                                    </label>
                                    <div class="less-than">
                                        *less than 1.5 Mb
                                    </div>
                                </div>
                                <div class="col-12 col-lg-6 ">
                                    <div class="g-recaptcha" data-sitekey="6LfoxEccAAAAAJ5bDVgtQAQ4Wrd008u5hZDgaXyq"></div>
                                </div>
                            </div>
                        </div>
                        <div class="text-align">
                            <button class="btn px-3 border-radius font-weight-bold section-ten__btn-send-message">
                                <div class="section-ten__text-send-message">
                                    SEND MESSAGE
                                </div>
                            </button>
                        </div>
                    </form>
                </div>
                <div class="col-12 col-lg-6 ">
                    <div class="row ">
                        <div class="col-6 section-ten__item-wrapper">
                            <div class="text-white section-ten__text-pages">Pages</div>
                            <ul class="p-0 list-none text-white ">
                                <li class="section-ten__text-pages-list">
                                    <a class="internal-link" data-href="#section-two">
                                        About Us
                                    </a>
                                </li>
                                <li class="section-ten__text-pages-list">
                                    <a class="internal-link" data-href="#section-five">
                                        Our Companies
                                    </a>
                                </li>
                                <li class="section-ten__text-pages-list">
                                    <a class="internal-link" data-href="#section-six">
                                        Our Clients
                                    </a>
                                </li>
                                <li class="section-ten__text-pages-list">
                                    <a class="internal-link" data-href="#section-seven">
                                        Our Services
                                    </a>
                                </li>
                                <li class="section-ten__text-pages-list">
                                    <a class="internal-link" data-href="#section-eight">
                                        Our Team
                                    </a>
                                </li>
                                <li class="section-ten__text-pages-list">
                                    <a class="internal-link" data-href="#section-ten">
                                        Contact
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div class="col-6 section-ten__item-wrapper">
                            <div class="text-white section-ten__text-pages">Location</div>
                            <div class="text-white mb-2 section-ten__text-pages-list location">F4Elements
                                <div>
                                    Jl. Jaha No.88, Cilandak Timur,
                                </div>
                                <div>
                                    Pasar Minggu, Jakarta Selatan, 12560
                                </div>
                            </div>
                        </div>
                        <div class="col-12 section-ten__item-wrapper">
                            <div class="f-mid text-white section-ten__text-pages">Inquiries</div>
                            <a class="text-white section-ten__text-pages-list" href="mailto:david@f4elements.com">david@f4elements.com</a>
                        </div>

                        <!-- <div class="col-12 mb-4 ">
                            <div class="f-cust f-mid text-white ">Subscribe</div>
                            <div class="text-white ">david@f4elements.com</div>
                        </div> -->

                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer bg-green">
        <div class="border-social">
            <!-- <div class="d-flex justify-content-between section-ten__socials-wrapper">
                <a href="#" class="section-ten__social-wrapper">
                    <img class="lazyload" data-src="public/landing/assets/image/social_media/facebook.svg" alt="" srcset="">
                </a>
                <a href="#" class="section-ten__social-wrapper">
                    <img class="lazyload" data-src="public/landing/assets/image/social_media/twitter.svg" alt="" srcset="">
                </a>
                <a href="#" class="section-ten__social-wrapper">
                    <img class="lazyload" data-src="public/landing/assets/image/social_media/instagram.svg" alt="" srcset="">
                </a>
                <a href="#" class="section-ten__social-wrapper">
                    <img class="lazyload" data-src="public/landing/assets/image/social_media/youtube.svg" alt="" srcset="">
                </a>
                <a href="#" class="section-ten__social-wrapper">
                    <img class="lazyload" data-src="public/landing/assets/image/social_media/linkedin.svg" alt="" srcset="">
                </a>
            </div> -->
        </div>
        <div class="text-center text-white section-ten__copyright">
            Ⓒ 2021 F4ELEMENTS, ALL RIGHTS RESERVED.
            <!-- <p class="text-koneksi-group">Powered by <a href="https://koneksi.group" target="_blank" class="koneksi-group">Koneksi Group</a></p> -->
        </div>
    </div>


    <div class="modal menu fade pr-0" id="menuModal" tabindex="-1" role="dialog" aria-labelledby="menuModalLabel" aria-hidden="true">
        <div class="modal-dialog m-width-unset  d-flex h-fill" role="document ">
            <div class="modal-content bg-dark overflow-hidden">
                <div class="close-container">
                    <div class="close-wrapper" data-dismiss="modal">
                    </div>
                </div>

                <div class="modal-body">
                    <div class="f4-wrapper">
                        <img data-src="public/landing/assets/image/logo/f4 shape 1.png" class="position-absolute left-0 lazyload" />
                    </div>
                    <div class="menu-wrapper">
                        <a href="#section-two" class="menu-item internal-link"> ABOUT US</a>
                        <a href="#section-five" class="menu-item internal-link"> OUR NETWORK</a>
                        <a href="#section-six" class="menu-item internal-link"> OUR CLIENT</a>
                        <a href="#section-seven" class="menu-item internal-link"> OUR SERVICES</a>
                        <a href="#section-eight" class="menu-item internal-link"> OUR TEAM</a>
                        <a href="#section-nine" class="menu-item internal-link"> CAREER</a>
                        <a href="#section-ten" class="menu-item internal-link"> CONTACT US </a>
                    </div>
                </div>
            </div>
        </div>
    </div>



    <?php
    if ($this->session->flashdata('message')) {
        $message = $this->session->flashdata('message');
        unset($_SESSION['message']);
    } else {
        $message = NULL;
    }
    ?>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="public/landing/js/jquery-3.6.0.min.js "></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="public/landing/js/bootstrap.min.js "></script>
    <script src="public/landing/js/main.js?version=20201-09-08"></script>
    <!--Start of Tawk.to Script-->
    <script type="text/javascript">
        var Tawk_API = Tawk_API || {},
            Tawk_LoadStart = new Date();
        (function() {
            var s1 = document.createElement("script"),
                s0 = document.getElementsByTagName("script")[0];
            s1.async = true;
            s1.src = 'https://embed.tawk.to/61348f09649e0a0a5cd4ae73/1feqktfgp';
            s1.charset = 'UTF-8';
            s1.setAttribute('crossorigin', '*');
            s0.parentNode.insertBefore(s1, s0);
        })();
    </script>
    <!--End of Tawk.to Script-->
    <script src="public/landing/js/lazysizes.min.js" async=""></script>

    <script>
        $('.internal-link').on('click', (e) => {
            $('.modal').modal('hide');
            // const target = $(e.target).data('href');
            // $('html, body').animate({
            //     scrollTop: $(target).offset().top
            // }, 1000)
            // document.querySelector(target).scrollIntoView({
            //     behavior: 'smooth'
            // })
            // window.location.href = target;
        });

        $('.internal-link-modal').on('click', (e) => {
            $('.modal').modal('hide');
        })
        $('.section-one__scroll-wrapper').on('click', (e) => {
            document.querySelector('#section-two').scrollIntoView({
                behavior: 'smooth'
            })
        })
        $('.have-chat').on('click', (e) => {
            document.querySelector('#section-ten').scrollIntoView({
                behavior: 'smooth'
            })
        })

        $('.scrollToBrand').on('click', (e) => {
            const tmp = $(e.target).closest('.modal.service')
            const id = tmp.attr('id')

            $('#' + id).animate({
                    scrollTop: $('#' + id + ' #scrollBrand').offset().top
                },
                500);
        })

        const message = "<?= $message; ?>";
        if (message) {
            alert(message)
        }

        $('body').on('scroll', function() {
            if ((($(".fixed-position-logo").offset().top > ($("#section-two").offset().top - 100) && $(".fixed-position-logo").offset().top < $("#section-five").offset().top - 100)) || ($(".fixed-position-logo").offset().top > ($("#section-ten").offset().top - 100))) {
                $('.fixed-position-logo').css('background-image', 'url(public/landing/assets/image/logo/logo-text-lg-white.svg)');
            } else {
                $('.fixed-position-logo').css('background-image', 'url(public/landing/assets/image/logo/logo-text-lg.svg)');
            }
        })

        $('#menuModal').on('shown.bs.modal', function(e) {
            $('.fixed-position-logo').css('background-image', 'url(public/landing/assets/image/logo/logo-text-f4-full-white.svg)');
        })
        $('#menuModal').on('hidden.bs.modal', function(e) {
            $('.fixed-position-logo').css('background-image', 'url(public/landing/assets/image/logo/logo-text-lg.svg)');
        })

        $('.fixed-position-logo').on('click', (e) => {
            $('.modal').modal('hide');
        })

        $(document).ready(function() {
            $(".section-six__wrapper-item").on("click", function(e) {
                $(".section-six__wrapper-item").removeClass('active')
                $(this).addClass('active')

                const target = $(this).attr('data-target');
                $(`.section-six__product-item`).hide()
                $(`.${target}`).css('display', 'flex')
            });
        });
    </script>
</body>
</body>

</html>