<!--app-content open-->
<div class="app-content">
    <div class="side-app">
        <!-- PAGE-HEADER -->
        <div class="page-header">
            <div>
                <h1 class="page-title">Inbox</h1>
            </div>
        </div>
        <!-- PAGE-HEADER END -->

        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header bg-transparent border-0">
                        <h3 class="card-title">Inbox List</h3>
                    </div>
                    <div class="">
                        <div class="grid-margin">
                            <div class="">
                                <div class="table-responsive">
                                    <table class="table card-table table-vcenter mb-0  align-items-center mb-0">
                                        <thead class="thead-light">
                                            <tr>
                                                <th>No</th>
                                                <th>Name</th>
                                                <th>Email</th>
                                                <th>Subject</th>
                                                <th>File</th>
                                                <th>Date</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            if (is_array($rows) || is_object($rows)) {
                                                foreach ($rows as $key => $row) { ?>
                                                    <tr data-id="<?= $row->id ?>">
                                                        <td><?= $key + 1; ?></td>
                                                        <td class="name"><?= $row->name; ?></td>
                                                        <td class="email"><?= $row->email; ?></td>
                                                        <td class="subject"><?= $row->subject; ?></td>
                                                        <td class="file">
                                                            <a target="_blank" href="<?= base_url($row->file_url); ?>"> See file</a>
                                                        </td>
                                                        <td class="date"><?= $row->created_at; ?></td>
                                                        <td class="">
                                                            <button class="btn btn-danger btn-del-msg" data-id="<?= $row->id; ?>">
                                                                <span><i class="fe fe-trash-2"></i></span>
                                                            </button>
                                                        </td>
                                                    </tr>
                                            <?php }
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
	const base_url = "<?= base_url() ?>";
</script>