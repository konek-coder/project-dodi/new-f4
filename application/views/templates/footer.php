			</div>			
			<!-- FOOTER -->
			<footer class="footer">
				<div class="container">
					<div class="row align-items-center flex-row-reverse">
						<div class="col-md-12 col-sm-12 text-center">
							Copyright © 2020 <a href="https://f4elements.com" target="_blank">F4Elements</a>. Designed by <a href="https://koneksi.group" target="_blank"> Koneksi Group </a> All rights reserved.
						</div>
					</div>
				</div>
			</footer>
			<!-- FOOTER CLOSED -->
		</div>

		<!-- BACK-TO-TOP -->
		<a href="#top" id="back-to-top"><i class="fa fa-angle-up"></i></a>

		<!-- JQUERY JS -->
		<script src="<?php echo $this->public?>/assets/js/jquery-3.4.1.min.js"></script>

		<!-- BOOTSTRAP JS -->
		<script src="<?php echo $this->public?>/assets/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
		<script src="<?php echo $this->public?>/assets/plugins/bootstrap/js/popper.min.js"></script>

		<!-- SPARKLINE JS-->
		<script src="<?php echo $this->public?>/assets/js/jquery.sparkline.min.js"></script>

		<!-- CHART-CIRCLE JS-->
		<script src="<?php echo $this->public?>/assets/js/circle-progress.min.js"></script>

		<!-- RATING STAR JS-->
		<script src="<?php echo $this->public?>/assets/plugins/rating/jquery.rating-stars.js"></script>

		<!-- EVA-ICONS JS -->
		<script src="<?php echo $this->public?>/assets/iconfonts/eva.min.js"></script>

		<!-- INPUT MASK JS-->
		<script src="<?php echo $this->public?>/assets/plugins/input-mask/jquery.mask.min.js"></script>

        <!-- SIDE-MENU JS-->
		<script src="<?php echo $this->public?>/assets/plugins/sidemenu/sidemenu.js"></script>

		<!-- PERFECT SCROLL BAR js-->
		<script src="<?php echo $this->public?>/assets/plugins/p-scroll/perfect-scrollbar.min.js"></script>
		<script src="<?php echo $this->public?>/assets/plugins/sidemenu/sidemenu-scroll.js"></script>

		<!-- CUSTOM SCROLL BAR JS-->
		<script src="<?php echo $this->public?>/assets/plugins/scroll-bar/jquery.mCustomScrollbar.concat.min.js"></script>

		<!-- SIDEBAR JS -->
		<script src="<?php echo $this->public?>/assets/plugins/sidebar/sidebar.js"></script>

		<!-- CUSTOM JS-->
		<script src="<?php echo $this->public?>/assets/js/custom.js"></script>
		
		<!-- SWAL -->
		<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
		<?php
			if(isset($page_js)){
				foreach ($page_js as $js) {
					print $js;
				}
			}
		?>
	</body>
</html>