<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->url=base_url();
		$this->load->library('form_validation');
    }

	public function index()
	{
		if ( $this->session->userdata('logged_in')){ 
            redirect('dashboard');
		}
		$this->form_validation->set_rules('email', 'Email', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required');
		
		if($this->form_validation->run()==false){
			$this->load->view('login');
		}else{
			$data = [
				'username'=> $this->input->post('email'),
				'password'=> $this->input->post('password'),
			];
			
			// validation here
			$isValid = false;
			if($data['username'] == 'david@f4elements.com' && $data['password'] == 'Admin@1234'){
				$isValid = true;
			}
			
			if($isValid){
				$this->_login($data['username']);
			}else{
				$this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Login Failed</div>');
				redirect('login');
			}
		}

	}

	private function _login($username){
		$newData = array(
			'username' => 'Admin',
			'name' => 'Admin',
			'role' => 'admin',
			'user_id' => '0',
			'logged_in' => TRUE
		);
		$this->session->set_userdata($newData);
		redirect('dashboard');
	}

	public function logout(){
		$array_items = array('username', 'name', 'email', 'role', 'user_id', 'logged_in');
		$this->session->unset_userdata($array_items);
		$msg = "You have been logged out";
		$this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">'.$msg.'</div>');
		redirect(base_url('login'));
	}
}
