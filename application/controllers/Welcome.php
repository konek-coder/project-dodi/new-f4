<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Welcome extends CI_Controller
{

	function __construct()
	{
		parent::__construct();
		$this->url = base_url();
		$this->public = base_url() . 'public';
		$this->load->model('m_dashboard');
		$this->res = base_url() . 'public/dist/js/';
		$this->load->helper('url', 'form');
		$this->load->library('form_validation');
		$this->load->library('session');
		$this->load->helper('new_helper');
	}

	public function index()
	{
		$careers = $this->m_dashboard->get_Career();
		$data['careers'] = $careers;
		$services = $this->m_dashboard->get_service();
		$data['services'] = $services;
		$this->load->view('welcome_message', $data);
	}

	public function storeMsg()
	{
		// validate captcha
		$captcha_response = trim($this->input->post('g-recaptcha-response'));
		$isValidCaptcha = $this->_isValidCaptcha($captcha_response);
		if ($isValidCaptcha == 1) {
			$data = array(
				'name'		=>	$this->input->post('name'),
				'email'		=>	$this->input->post('email'),
				'subject'	=>	$this->input->post('subject')
			);
			$data['file_url'] = $this->_uploadImg();
			$isProcessOK = $this->m_dashboard->store_message($data);
			if (!$isProcessOK) {
				$this->session->set_flashdata('message', 'An error occured. Please try again later. Error Code: 501');
				redirect(base_url());
			} else {
				$this->session->set_flashdata('message', 'Data Stored Successfully');
				redirect(base_url());
			}
		} else {
			$this->session->set_flashdata('message', $isValidCaptcha);
			redirect(base_url());
		}
	}

	private function _uploadImg()
	{
		$nama = 'file_' . time();
		$path = './upload/visitor';
		$config['upload_path'] = $path;
		$config['allowed_types'] = '*';
		// $config['max_size'] = 2000;
		$config['file_name'] = $nama;

		$this->upload->initialize($config);
		$this->upload->do_upload('fileToUpload');

		$file_url = $path . '/' . $this->upload->data()['file_name'];

		return $file_url;
	}

	private function _isValidCaptcha($captcha_response)
	{
		if ($captcha_response != '') {
			$keySecret = '6Ld1p_obAAAAAC92MxE8cCz2TnSaWCMwxaZtwide';

			$check = array(
				'secret'		=>	$keySecret,
				'response'		=>	$this->input->post('g-recaptcha-response')
			);

			$startProcess = curl_init();

			curl_setopt($startProcess, CURLOPT_URL, "https://www.google.com/recaptcha/api/siteverify");

			curl_setopt($startProcess, CURLOPT_POST, true);

			curl_setopt($startProcess, CURLOPT_POSTFIELDS, http_build_query($check));

			curl_setopt($startProcess, CURLOPT_SSL_VERIFYPEER, false);

			curl_setopt($startProcess, CURLOPT_RETURNTRANSFER, true);

			$receiveData = curl_exec($startProcess);

			$finalResponse = json_decode($receiveData, true);
			if ($finalResponse['success']) {
				return 1;
			} else {
				return 'Repatcha Failed.';
			}
		} else {
			return 'Please fill recaptcha.';
		}
	}

	public function setLang($cookie_value)
	{
		$cookie_name = "lang";
		setcookie($cookie_name, $cookie_value, time() + (86400 * 30), "/");
		redirect(base_url());
	}
}
