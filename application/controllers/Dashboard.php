<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Dashboard extends CI_Controller
{

	function __construct()
	{
		parent::__construct();
		$this->url = base_url();
		$this->public = base_url() . 'public';
		$this->load->model('m_dashboard');
		$this->res = base_url() . 'public/dist/js/';
		$this->load->helper('url', 'form');
		$this->load->library('form_validation');
		if (!$this->session->userdata('logged_in')) {
			redirect('login');
		}
	}

	public function index()
	{
		$this->load->view('templates/header');
		$this->load->view('dashboard');
		$this->load->view('templates/footer');
	}

	public function career()
	{
		$career = $this->m_dashboard->get_Career();
		$data['rows'] = $career;

		// load javascript
		$data['page_js'] = array(
			'<!--This page js -->',
			'<script src="' . $this->res . 'career.js"></script>'
		);
		$this->load->view('templates/header');
		$this->load->view('career', $data);
		$this->load->view('templates/footer');
	}

	public function createCareer()
	{
		$inputs = $this->input->post();
		$isInsert = $this->m_dashboard->create_career($inputs);
		if (!$isInsert) {
			echo 'An Error has occured. Please try again. Error Code: 501';
		} else {
			redirect('dashboard/career');
		}
	}

	public function updateCareer($id)
	{
		$inputs = $this->input->post();
		$isProcessOK = $this->m_dashboard->edit_career($id, $inputs);
		if (!$isProcessOK) {
			echo 'An Error has occured. Please try again. Error Code: 501';
		} else {
			redirect('dashboard/career');
		}
	}

	public function deleteCareer($id)
	{
		$isProcessOK = $this->m_dashboard->delete_career($id);
		if (!$isProcessOK) {
			echo 'An Error has occured. Please try again. Error Code: 501';
		} else {
			redirect('dashboard/career');
		}
	}

	public function services()
	{
		$rows = $this->m_dashboard->get_service();
		$data['rows'] = $rows;
		// load javascript
		$data['page_js'] = array(
			'<!--This page js -->',
			'<script src="' . $this->res . 'service.js"></script>'
		);
		$this->load->view('templates/header');
		$this->load->view('services', $data);
		$this->load->view('templates/footer');
	}

	public function createService()
	{
		$inputs = $this->input->post();
		$isInsert = $this->m_dashboard->create_service($inputs);
		if (!$isInsert) {
			echo 'An Error has occured. Please try again. Error Code: 501';
		} else {
			redirect('dashboard/services');
		}
	}

	public function updateService($id)
	{
		$inputs = $this->input->post();

		if (!empty($_FILES['foto-logo']['name'])) {
			$nama = 'file_' . time();
			$path = './upload/service/' . $id;
			$config['upload_path'] = $path;
			$config['allowed_types'] = 'gif|jpg|png';
			$config['max_size'] = 2000;
			$config['file_name'] = $nama;
			$this->upload->initialize($config);
			$this->upload->do_upload('foto-logo');
			$inputs['logo'] = $nama . $this->upload->data('file_ext');
			// $image_url = $path . '/' . $this->upload->data()['file_name'];
		}

		$uploadedFiles = [];
		$count = count($_FILES['foto-images']['name']);

		for ($i = 0; $i < $count; $i++) {
			if (!empty($_FILES['foto-images']['name'][$i])) {
				$_FILES['file']['name'] = $_FILES['foto-images']['name'][$i];
				$_FILES['file']['type'] = $_FILES['foto-images']['type'][$i];
				$_FILES['file']['tmp_name'] = $_FILES['foto-images']['tmp_name'][$i];
				$_FILES['file']['error'] = $_FILES['foto-images']['error'][$i];
				$_FILES['file']['size'] = $_FILES['foto-images']['size'][$i];

				$nama = 'file_' . time() . rand(10, 100);
				$path = './upload/service/' . $id;

				$config['upload_path'] = $path;
				$config['allowed_types'] = 'gif|jpg|png';
				$config['max_size'] = 2000;
				$config['file_name'] =  $nama;

				$this->upload->initialize($config);
				$this->upload->do_upload('file');

				$tmpName = $nama . $this->upload->data('file_ext');
				array_push($uploadedFiles, $tmpName);
			}
		}
		if (count($uploadedFiles) > 0) {
			$inputs['images'] = json_encode($uploadedFiles);
		}

		$isProcessOK = $this->m_dashboard->edit_service($id, $inputs);
		if (!$isProcessOK) {
			echo 'An Error has occured. Please try again. Error Code: 501';
		} else {
			redirect('dashboard/services');
		}
	}

	public function deleteService($id)
	{
		$isProcessOK = $this->m_dashboard->delete_service($id);
		if (!$isProcessOK) {
			echo 'An Error has occured. Please try again. Error Code: 501';
		} else {
			redirect('dashboard/services');
		}
	}

	public function togglePublishService($id)
	{
		$isProcessOK = $this->m_dashboard->toggle_publish_service($id);
		if (!$isProcessOK) {
			echo 'An Error has occured. Please try again. Error Code: 501';
		} else {
			redirect('dashboard/services');
		}
	}

	public function inbox()
	{
		$rows = $this->m_dashboard->get_message();
		$data['rows'] = $rows;
		// load javascript
		$data['page_js'] = array(
			'<!--This page js -->',
			'<script src="' . $this->res . 'inbox.js"></script>'
		);
		$this->load->view('templates/header');
		$this->load->view('inbox', $data);
		$this->load->view('templates/footer');
	}

	public function deleteInbox($id)
	{
		$isProcessOK = $this->m_dashboard->delete_message($id);
		if (!$isProcessOK) {
			echo 'An Error has occured. Please try again. Error Code: 501';
		} else {
			redirect('dashboard/inbox');
		}
	}
}
