<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

if (!function_exists('language_solver')) {
    function language_solver($key)
    {  
        // read cookies and set selected language
        $languageCode = 'en';
        if(isset($_COOKIE['lang'])){
            $languageCode = $_COOKIE['lang'];
        }

        // read json in public/lang
        $fileLocation = base_url() . 'public/lang/' . $languageCode . '.json';
        $fileData = file_get_contents($fileLocation);

        // convert json to php array associative
        $phpJson = json_decode($fileData, true);

        // print value
        if (isset($phpJson[$key]))
            echo $phpJson[$key];
    }
}
