-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 24, 2021 at 08:37 AM
-- Server version: 10.4.17-MariaDB
-- PHP Version: 8.0.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `f4elements`
--

-- --------------------------------------------------------

--
-- Table structure for table `career`
--

CREATE TABLE `career` (
  `id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `job_code` varchar(100) NOT NULL,
  `job_desc` longtext NOT NULL,
  `requirements` longtext NOT NULL,
  `job_location` varchar(100) NOT NULL,
  `salary` varchar(100) NOT NULL,
  `summary` longtext NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp(),
  `is_deleted` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `career`
--

INSERT INTO `career` (`id`, `title`, `job_code`, `job_desc`, `requirements`, `job_location`, `salary`, `summary`, `created_at`, `updated_at`, `is_deleted`) VALUES
(1, 'Internship', 'account', '[\"Responsible as liaison for creating and maintaining trust and strong relationship with clients and all stakeholders.\", \"Build and maintain relationship with client by ensuring all client needs are met.\", \"Assist clients in all project activities.\", \"Provide digital project consultancy as needed for clients.\", \"Manage all projects administration (include payment status).\", \"Responsible account activities comply with company policy & procedure.\", \"Responsible high quality digital products and services based on project requirements.\", \"Responsible to brief internal team about the agreements with client.\"]', '[\"Bachelor degree from any field, communication is preferable.\", \"Fresh graduates are welcome to apply.\", \"Responsible and Confident.\", \"Excellent communication and interpersonal skills.\", \"Up to date to creative and technology trends.\"]', 'Jakarta Selatan', 'Based on experience', 'We inviting you all digital enthusiasts to explore many brands on different industry. We have a regular skill-upgrade session in our office. If you\'re as passionate about the digital industry as we do, then you might just be the one we\'re looking for.', '2021-08-01 15:45:08', '2021-08-11 02:06:18', NULL),
(2, 'Freelance', 'account', '[\"Responsible as liaison for creating and maintaining trust and strong relationship with clients and all stakeholders.\", \"Build and maintain relationship with client by ensuring all client needs are met.\", \"Assist clients in all project activities.\", \"Provide digital project consultancy as needed for clients.\", \"Manage all projects administration (include payment status).\", \"Responsible account activities comply with company policy & procedure.\", \"Responsible high quality digital products and services based on project requirements.\", \"Responsible to brief internal team about the agreements with client.\"]', '[\"Bachelor degree from any field, communication is preferable.\", \"Fresh graduates are welcome to apply.\", \"Responsible and Confident.\", \"Excellent communication and interpersonal skills.\", \"Up to date to creative and technology trends.\"]', 'Jakarta Selatan', 'Based on experience', 'We inviting you all digital enthusiasts to explore many brands on different industry. We have a regular skill-upgrade session in our office. If you\'re as passionate about the digital industry as we do, then you might just be the one we\'re looking for.', '2021-08-01 15:45:08', '2021-08-01 15:45:08', NULL),
(3, 'Digital Designer', 'account', '[\"Responsible as liaison for creating and maintaining trust and strong relationship with clients and all stakeholders.\", \"Build and maintain relationship with client by ensuring all client needs are met.\", \"Assist clients in all project activities.\", \"Provide digital project consultancy as needed for clients.\", \"Manage all projects administration (include payment status).\", \"Responsible account activities comply with company policy & procedure.\", \"Responsible high quality digital products and services based on project requirements.\", \"Responsible to brief internal team about the agreements with client.\"]', '[\"Bachelor degree from any field, communication is preferable.\", \"Fresh graduates are welcome to apply.\", \"Responsible and Confident.\", \"Excellent communication and interpersonal skills.\", \"Up to date to creative and technology trends.\"]', 'Jakarta Selatan', 'Based on experience', 'We inviting you all digital enthusiasts to explore many brands on different industry. We have a regular skill-upgrade session in our office. If you\'re as passionate about the digital industry as we do, then you might just be the one we\'re looking for.', '2021-08-01 15:45:08', '2021-08-01 15:45:08', NULL),
(4, 'Social Media Specialist', 'account', '[\"Responsible as liaison for creating and maintaining trust and strong relationship with clients and all stakeholders.\", \"Build and maintain relationship with client by ensuring all client needs are met.\", \"Assist clients in all project activities.\", \"Provide digital project consultancy as needed for clients.\", \"Manage all projects administration (include payment status).\", \"Responsible account activities comply with company policy & procedure.\", \"Responsible high quality digital products and services based on project requirements.\", \"Responsible to brief internal team about the agreements with client.\"]', '[\"Bachelor degree from any field, communication is preferable.\", \"Fresh graduates are welcome to apply.\", \"Responsible and Confident.\", \"Excellent communication and interpersonal skills.\", \"Up to date to creative and technology trends.\"]', 'Jakarta Selatan', 'Based on experience', 'We inviting you all digital enthusiasts to explore many brands on different industry. We have a regular skill-upgrade session in our office. If you\'re as passionate about the digital industry as we do, then you might just be the one we\'re looking for.', '2021-08-01 15:45:08', '2021-08-11 02:06:38', NULL),
(5, 'Marketplace Specialist', 'account', '[\"Responsible as liaison for creating and maintaining trust and strong relationship with clients and all stakeholders.\", \"Build and maintain relationship with client by ensuring all client needs are met.\", \"Assist clients in all project activities.\", \"Provide digital project consultancy as needed for clients.\", \"Manage all projects administration (include payment status).\", \"Responsible account activities comply with company policy & procedure.\", \"Responsible high quality digital products and services based on project requirements.\", \"Responsible to brief internal team about the agreements with client.\"]', '[\"Bachelor degree from any field, communication is preferable.\", \"Fresh graduates are welcome to apply.\", \"Responsible and Confident.\", \"Excellent communication and interpersonal skills.\", \"Up to date to creative and technology trends.\"]', 'Jakarta Selatan', 'Based on experience', 'We inviting you all digital enthusiasts to explore many brands on different industry. We have a regular skill-upgrade session in our office. If you\'re as passionate about the digital industry as we do, then you might just be the one we\'re looking for.', '2021-08-01 15:45:08', '2021-08-11 02:06:53', NULL),
(6, 'Art Director', 'account', '[\"Responsible as liaison for creating and maintaining trust and strong relationship with clients and all stakeholders.\", \"Build and maintain relationship with client by ensuring all client needs are met.\", \"Assist clients in all project activities.\", \"Provide digital project consultancy as needed for clients.\", \"Manage all projects administration (include payment status).\", \"Responsible account activities comply with company policy & procedure.\", \"Responsible high quality digital products and services based on project requirements.\", \"Responsible to brief internal team about the agreements with client.\"]', '[\"Bachelor degree from any field, communication is preferable.\", \"Fresh graduates are welcome to apply.\", \"Responsible and Confident.\", \"Excellent communication and interpersonal skills.\", \"Up to date to creative and technology trends.\"]', 'Jakarta Selatan', 'Based on experience', 'We inviting you all digital enthusiasts to explore many brands on different industry. We have a regular skill-upgrade session in our office. If you\'re as passionate about the digital industry as we do, then you might just be the one we\'re looking for.', '2021-08-01 15:45:08', '2021-08-11 02:07:02', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `message`
--

CREATE TABLE `message` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `subject` longtext NOT NULL,
  `file_url` varchar(100) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp(),
  `is_deleted` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `service`
--

CREATE TABLE `service` (
  `id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `code` varchar(100) NOT NULL,
  `popup_title` varchar(255) NOT NULL,
  `logo` varchar(100) NOT NULL,
  `brand_name` varchar(100) NOT NULL,
  `overview` longtext NOT NULL,
  `others` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL CHECK (json_valid(`others`)),
  `capability` varchar(255) NOT NULL,
  `team` varchar(255) NOT NULL,
  `year` varchar(255) NOT NULL,
  `website` varchar(255) NOT NULL,
  `images` longtext NOT NULL,
  `is_published` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp(),
  `is_deleted` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `service`
--

INSERT INTO `service` (`id`, `title`, `code`, `popup_title`, `logo`, `brand_name`, `overview`, `others`, `capability`, `team`, `year`, `website`, `images`, `is_published`, `created_at`, `updated_at`, `is_deleted`) VALUES
(1, 'brand campaign																																																																						', 'brand-campaign', 'Influencers campaign, digital activation', 'file_1628983849.png', 'SAJIKU PREMIX', 'Sajiku as subsidiary of ajinomoto, provide product like sauces and instant premix coating mix and seasoning.\r\n\r\nWe work with them to create two interesting concept.\r\n\r\nFor Sajiku tepung Bakwan we create a digital quiz and inviting people to create their best version of bakwan with sajiku bakwan, the best winner can share their recipe and appear on an Instagram live session with several mega KOL such as Adelia Pasha\r\n\r\nFor Sajiku premix seasonings we create a Instagram filter game, where people can interact and collect the highest score and work with KOL to activate the quiz', '{\"capability\":\"Advertising\", \"team\":\"Interface Sunrise\", \"year\":\"2020\", \"website\":\"https://youtube.com/watch?time_continue=4&v=Is1ltAKGFTY&feature=emb_title\"}', 'Advertising', 'Interface Sunrise', '2020', 'https://youtube.com/watch?time_continue=4&v=Is1ltAKGFTY&feature=emb_title', '[\"file_162898394821.png\",\"file_162898394872.png\",\"file_162898394879.png\",\"file_162898394813.png\"]', 1, '2021-08-02 00:53:51', '2021-08-15 06:32:28', NULL),
(2, 'gathering & exhibitions', 'gathering', 'Events, exhibitions, launching, gatherings, road shows, race event', 'logo.svg', 'NIKE SG vs KL 10K', 'Nike SG vs KL 10 K is a concept where two rival cities try to beat each other, where at the same day they try to collect KM via the nikerunning app. This event was a logistic madness where the NIKE South East Asia Fied Marketing team must work with at least 10 agencies in each city and closed down iconic routes in two busiest city Singapore and Kuala Lumpur at the same day, same minutes where the race starts starts at the same time.', '{\"capability\":\"Advertising\", \"team\":\"Interface Sunrise\", \"year\":\"2020\", \"website\":\"youtube.com/watch?time_continue=4&v=Is1ltAKGFTY&feature=emb_title\"}', 'Advertising', 'Interface Sunrise', '2020', 'https://youtube.com/watch?time_continue=4&v=Is1ltAKGFTY&feature=emb_title', '[\"1.png\",\"2.png\",\"3.png\",\"4.png\"]', 1, '2021-08-02 00:53:51', '2021-08-02 00:53:51', NULL),
(3, 'virtual / webinar programs', 'virtual', 'Virtual Event', 'logo.png', 'Harris Virtual Race', 'When the pandemic Hits, Harris Day (Harris’s Hotel annual event need to change concept to virtual). We create a two in one race concept race with virtuathlon as the race platform, combining running and cycling where people all around Indonesia can compete with each other in one month virtual race', '{\"capability\":\"Advertising\", \"team\":\"Interface Sunrise\", \"year\":\"2020\", \"website\":\"youtube.com/watch?time_continue=4&v=Is1ltAKGFTY&feature=emb_title\"}', 'Advertising', 'Interface Sunrise', '2020', 'https://youtube.com/watch?time_continue=4&v=Is1ltAKGFTY&feature=emb_title', '[\"1.png\",\"2.png\",\"3.png\",\"4.png\"]', 1, '2021-08-02 01:09:55', '2021-08-02 01:14:45', NULL),
(4, 'Video & animation																																																																																			', 'video-animation', 'VIDEO & ANIMATION', 'file_1628985252.png', 'Video & Animation', 'When the pandemic Hits, Harris Day (Harris’s Hotel annual event need to change concept to virtual). We create a two in one race concept race with virtuathlon as the race platform, combining running and cycling where people all around Indonesia can compete with each other in one month virtual race', '{\"capability\":\"Advertising\", \"team\":\"Interface Sunrise\", \"year\":\"2020\", \"website\":\"youtube.com/watch?time_continue=4&v=Is1ltAKGFTY&feature=emb_title\"}', 'Advertising', 'Interface Sunrise', '2020', 'https://youtube.com/watch?time_continue=4&v=Is1ltAKGFTY&feature=emb_title', '[\"file_162898529866.png\"]', 1, '2021-08-02 01:14:11', '2021-08-15 06:54:58', NULL),
(5, 'posm branding & stores dress up																																																																					', 'posm-branding', 'VIDEO & ANIMATION', '', 'POSM Branding & Stores Dress Up', 'When the pandemic Hits, Harris Day (Harris’s Hotel annual event need to change concept to virtual). We create a two in one race concept race with virtuathlon as the race platform, combining running and cycling where people all around Indonesia can compete with each other in one month virtual race', '{\"capability\":\"Advertising\", \"team\":\"Interface Sunrise\", \"year\":\"2020\", \"website\":\"youtube.com/watch?time_continue=4&v=Is1ltAKGFTY&feature=emb_title\"}', 'Advertising', 'Interface Sunrise', '2020', 'https://youtube.com/watch?time_continue=4&v=Is1ltAKGFTY&feature=emb_title', '[\"file_162960653127.png\",\"file_162960653135.png\"]', 1, '2021-08-02 01:17:59', '2021-08-22 11:28:51', NULL),
(6, 'kol & social media management																																										', 'kol-social', 'VIDEO & ANIMATION', '', 'KOL & Social Media Management', 'When the pandemic Hits, Harris Day (Harris’s Hotel annual event need to change concept to virtual). We create a two in one race concept race with virtuathlon as the race platform, combining running and cycling where people all around Indonesia can compete with each other in one month virtual race', '{\"capability\":\"Advertising\", \"team\":\"Interface Sunrise\", \"year\":\"2020\", \"website\":\"youtube.com/watch?time_continue=4&v=Is1ltAKGFTY&feature=emb_title\"}', 'Advertising', 'Interface Sunrise', '2020', 'https://youtube.com/watch?time_continue=4&v=Is1ltAKGFTY&feature=emb_title', '[\"file_162960454738.png\",\"file_162960454733.png\"]', 1, '2021-08-02 01:19:12', '2021-08-22 10:55:47', NULL),
(7, 'trade & in-store engagement', 'trade', 'VIDEO & ANIMATION', '', 'Trade & In-Store Engagement', 'When the pandemic Hits, Harris Day (Harris’s Hotel annual event need to change concept to virtual). We create a two in one race concept race with virtuathlon as the race platform, combining running and cycling where people all around Indonesia can compete with each other in one month virtual race', '{\"capability\":\"Advertising\", \"team\":\"Interface Sunrise\", \"year\":\"2020\", \"website\":\"youtube.com/watch?time_continue=4&v=Is1ltAKGFTY&feature=emb_title\"}', 'Advertising', 'Interface Sunrise', '2020', 'https://youtube.com/watch?time_continue=4&v=Is1ltAKGFTY&feature=emb_title', '[]', 1, '2021-08-02 01:19:54', '2021-08-02 01:19:54', NULL),
(8, 'channel-leads (pr, digital, trade)', 'channel', 'VIDEO & ANIMATION', '', 'Channel Leads', 'When the pandemic Hits, Harris Day (Harris’s Hotel annual event need to change concept to virtual). We create a two in one race concept race with virtuathlon as the race platform, combining running and cycling where people all around Indonesia can compete with each other in one month virtual race', '{\"capability\":\"Advertising\", \"team\":\"Interface Sunrise\", \"year\":\"2020\", \"website\":\"youtube.com/watch?time_continue=4&v=Is1ltAKGFTY&feature=emb_title\"}', 'Advertising', 'Interface Sunrise', '2020', 'https://youtube.com/watch?time_continue=4&v=Is1ltAKGFTY&feature=emb_title', '[]', 1, '2021-08-02 01:19:54', '2021-08-02 01:19:54', NULL),
(9, 'corporate & government project', 'corporate', 'Corporate & Government Project', '', 'Brand Name Here', 'When the pandemic Hits, Harris Day (Harris’s Hotel annual event need to change concept to virtual). We create a two in one race concept race with virtuathlon as the race platform, combining running and cycling where people all around Indonesia can compete with each other in one month virtual race', '{\"capability\":\"Advertising\", \"team\":\"Interface Sunrise\", \"year\":\"2020\", \"website\":\"youtube.com/watch?time_continue=4&v=Is1ltAKGFTY&feature=emb_title\"}', 'Advertising', 'Interface Sunrise', '2020', 'https://youtube.com/watch?time_continue=4&v=Is1ltAKGFTY&feature=emb_title', '[]', 1, '2021-08-02 01:19:54', '2021-08-02 01:19:54', NULL),
(10, 'scientific project', 'scientific-project', 'Scientific Project', '', 'Brand Name Here', 'When the pandemic Hits, Harris Day (Harris’s Hotel annual event need to change concept to virtual). We create a two in one race concept race with virtuathlon as the race platform, combining running and cycling where people all around Indonesia can compete with each other in one month virtual race', '{\"capability\":\"Advertising\", \"team\":\"Interface Sunrise\", \"year\":\"2020\", \"website\":\"youtube.com/watch?time_continue=4&v=Is1ltAKGFTY&feature=emb_title\"}', 'Advertising', 'Interface Sunrise', '2020', 'https://youtube.com/watch?time_continue=4&v=Is1ltAKGFTY&feature=emb_title', '[]', 1, '2021-08-02 01:19:54', '2021-08-02 01:19:54', NULL),
(11, 'printing', 'printing', 'Printing', '', 'Brand Name Here', 'When the pandemic Hits, Harris Day (Harris’s Hotel annual event need to change concept to virtual). We create a two in one race concept race with virtuathlon as the race platform, combining running and cycling where people all around Indonesia can compete with each other in one month virtual race', '{\"capability\":\"Advertising\", \"team\":\"Interface Sunrise\", \"year\":\"2020\", \"website\":\"youtube.com/watch?time_continue=4&v=Is1ltAKGFTY&feature=emb_title\"}', 'Advertising', 'Interface Sunrise', '2020', 'https://youtube.com/watch?time_continue=4&v=Is1ltAKGFTY&feature=emb_title', '[]', 1, '2021-08-02 01:19:54', '2021-08-02 01:19:54', NULL),
(12, 'production workshop', 'production-workshop', 'Production Workshop', '', 'Brand Name Here', 'When the pandemic Hits, Harris Day (Harris’s Hotel annual event need to change concept to virtual). We create a two in one race concept race with virtuathlon as the race platform, combining running and cycling where people all around Indonesia can compete with each other in one month virtual race', '{\"capability\":\"Advertising\", \"team\":\"Interface Sunrise\", \"year\":\"2020\", \"website\":\"youtube.com/watch?time_continue=4&v=Is1ltAKGFTY&feature=emb_title\"}', 'Advertising', 'Interface Sunrise', '2020', 'https://youtube.com/watch?time_continue=4&v=Is1ltAKGFTY&feature=emb_title', '[]', 1, '2021-08-02 01:19:54', '2021-08-02 01:19:54', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `career`
--
ALTER TABLE `career`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `message`
--
ALTER TABLE `message`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `service`
--
ALTER TABLE `service`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `career`
--
ALTER TABLE `career`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `message`
--
ALTER TABLE `message`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `service`
--
ALTER TABLE `service`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
